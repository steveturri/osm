@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Categories</h1>
  <div>
  	{!!$results!!}
  </div>
</div>
</div>
@endsection