@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Articles</h1>
    @if(isset($category))
    	<h3>{{$category->name}}</h3>
    @endif
  <div>
  @foreach($articles as $article)
  	<p>
  		<a href="{{$article->link}}" target="_blank">{{ $article->title }}</a> ({{$article->publish_date->format('m/d/Y')}})<br />
  		{{$article->description}}
	</p>
  @endforeach
  </div>
</div>
</div>
@endsection