<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category=NULL)
    {        
        if($category) {
            $category_record = Category::where('path', $category)->first();
            $data['articles'] = Article::where('category_id', $category_record->id)->get();
            $data['category'] = $category_record;
        } else {
            $data['articles'] = Article::all();            
        }
        

        return view('article.index', $data);
    }
}
