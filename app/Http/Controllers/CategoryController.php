<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data['categories'] = DB::table('categories')->selectRaw("categories.*,count(articles.id) as total")->where('active', true)->join('articles', 'articles.category_id', '=', 'categories.id')->groupBy('categories.id')->get();                
        return view('category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'path'=>'required',
        ]);

        $category = new Category([
            'name' => $request->get('name'),
            'path' => $request->get('path'),
            'active' => true,
        ]);
        $category->save();
        return redirect('/categories')->with('success', 'Category saved!');
    }

}
