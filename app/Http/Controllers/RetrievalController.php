<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Willvincent\Feeds;
use App\Category;
use App\Article;
use App\Retrieval;

class RetrievalController extends Controller
{
    public function retrieve_articles()
    {
    	$categories = Category::all();
    	$data['results'] = '';
    	foreach ($categories as $category) {
	    	$url = env("RSS_BASE") . $category->path . '/rss.xml';
	    	$feed = \Feeds::make($url);
		    $items = $feed->get_items();
		    $records = 0;

		    foreach($items as $item) {
		        $title = $item->get_title();
		        $link = $item->get_permalink();
		        $description = $item->get_description();
		        if(!$description) {
		        	$description = '';
		        }
		        $guid_array = $item->get_item_tags('','guid');
		        $date_array = $item->get_item_tags('','pubDate');
		        $guid =  $guid_array[0]['data'];
		        $date =  $date_array[0]['data'];

		        //Check if Exists already
		        $existing = Article::where('guid', $guid)->first();	        
		        
		        if(!$existing) {
		        	try {
		        		$article = new Article;
			        	$article->category_id = $category->id;
			        	$article->title = $title;
			        	$article->link = $link;
			        	$article->description = $description;
			        	$article->guid = $guid;
			        	$article->publish_date = date('Y-m-d', strtotime($date));
			        	$article->save();	
			        	$records++;
		        	} catch (\Exception $e) {
					    return $e->getMessage();
					}
		        }
		    }

		    //Update retrieval log
	        $retrieval = new Retrieval;
	        $retrieval->category_id = $category->id;
	        $retrieval->new_records = $records;
	        $retrieval->requested_by = 'Authorized User';
	        $retrieval->save();
	        
	        $data['results'] .= $records . ' articles added for ' . $category->name . '<br/>';
	        
		}
		return view('retrieval.completed', $data);
	}
}
