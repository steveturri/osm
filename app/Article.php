<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'category_id','title', 'link', 'description', 'guid', 'publish_date'
    ];

    protected $dates = [
        'publish_date',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
